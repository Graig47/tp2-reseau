# I.ECHANGE ARP

## Générer des requêtes ARP


```
user@MacBook-Pro-de-User ~ % ssh graig@10.2.1.11
graig@10.2.1.11's password: 
Activate the web console with: systemctl enable --now cockpit.socket

Last login: Mon Sep 20 13:09:32 2021 from 10.2.1.1
[graig@node1 ~]$ ping 10.2.1.12
PING 10.2.1.12 (10.2.1.12) 56(84) bytes of data.
64 bytes from 10.2.1.12: icmp_seq=1 ttl=64 time=0.637 ms
64 bytes from 10.2.1.12: icmp_seq=2 ttl=64 time=0.530 ms
64 bytes from 10.2.1.12: icmp_seq=3 ttl=64 time=0.835 ms

user@MacBook-Pro-de-User ~ % ssh graig@10.2.1.12
graig@10.2.1.12's password: 
Activate the web console with: systemctl enable --now cockpit.socket

Last login: Mon Sep 20 13:09:52 2021 from 10.2.1.1
[graig@node2 ~]$ ping 10.2.1.11
PING 10.2.1.11 (10.2.1.11) 56(84) bytes of data.
64 bytes from 10.2.1.11: icmp_seq=1 ttl=64 time=1.16 ms
64 bytes from 10.2.1.11: icmp_seq=2 ttl=64 time=0.767 ms
```

```
[graig@node1 ~]$ arp -a
? (10.2.1.1) at 0a:00:27:00:00:01 [ether] on enp0s8
_gateway (10.0.2.2) at 52:54:00:12:35:02 [ether] on enp0s3
? (10.2.1.12) at 08:00:27:11:c0:d6 [ether] on enp0s8

[graig@node2 ~]$ arp -a
? (10.2.1.1) at 0a:00:27:00:00:01 [ether] on enp0s8
_gateway (10.0.2.2) at 52:54:00:12:35:02 [ether] on enp0s3
? (10.2.1.11) at 08:00:27:9a:ca:3f [ether] on enp0s8
```

```
[graig@node1 ~]$ arp -a
? (10.2.1.1) at 0a:00:27:00:00:01 [ether] on enp0s8
_gateway (10.0.2.2) at 52:54:00:12:35:02 [ether] on enp0s3
? (10.2.1.12) at 08:00:27:11:c0:d6 [ether] on enp0s8

[graig@node2 ~]$ arp -a
? (10.2.1.1) at 0a:00:27:00:00:01 [ether] on enp0s8
_gateway (10.0.2.2) at 52:54:00:12:35:02 [ether] on enp0s3
? (10.2.1.11) at 08:00:27:9a:ca:3f [ether] on enp0s8
```

# 2.Analyse de trames

## Analyse de trames

```
[graig@node1 ~]$ sudo ip -s -s neigh flush all
[sudo] Mot de passe de graig : 
10.2.1.1 dev enp0s8 lladdr 0a:00:27:00:00:01 ref 1 used 10/0/10 probes 4 REACHABLE
10.0.2.2 dev enp0s3 lladdr 52:54:00:12:35:02 used 168/163/131 probes 1 STALE
10.2.1.12 dev enp0s8 lladdr 08:00:27:11:c0:d6 used 173/173/146 probes 4 STALE

*** Round 1, deleting 3 entries ***
*** Flush is complete after 1 round ***
[graig@node1 ~]$ arp -a
? (10.2.1.1) at 0a:00:27:00:00:01 [ether] on enp0s8
_gateway (10.0.2.2) at 52:54:00:12:35:02 [ether] on enp0s3
[graig@node1 ~]$ ping 10.2.1.12
PING 10.2.1.12 (10.2.1.12) 56(84) bytes of data.
64 bytes from 10.2.1.12: icmp_seq=1 ttl=64 time=0.795 ms
64 bytes from 10.2.1.12: icmp_seq=2 ttl=64 time=0.532 ms
64 bytes from 10.2.1.12: icmp_seq=3 ttl=64 time=0.518 ms
64 bytes from 10.2.1.12: icmp_seq=4 ttl=64 time=0.852 ms
64 bytes from 10.2.1.12: icmp_seq=5 ttl=64 time=0.648 ms
^C
--- 10.2.1.12 ping statistics ---
5 packets transmitted, 5 received, 0% packet loss, time 4102ms
rtt min/avg/max/mdev = 0.518/0.669/0.852/0.135 ms
[graig@node1 ~]$ 
```

```
[graig@node2 ~]$ sudo ip -s -s neigh flush all
[sudo] Mot de passe de graig : 
10.2.1.1 dev enp0s8 lladdr 0a:00:27:00:00:01 ref 1 used 39/0/38 probes 0 REACHABLE
10.0.2.2 dev enp0s3 lladdr 52:54:00:12:35:02 used 1291/1279/1242 probes 1 STALE
10.2.1.11 dev enp0s8 lladdr 08:00:27:9a:ca:3f used 188/185/165 probes 1 STALE

*** Round 1, deleting 3 entries ***
*** Flush is complete after 1 round ***
[graig@node2 ~]$ arp -a
? (10.2.1.1) at 0a:00:27:00:00:01 [ether] on enp0s8
_gateway (10.0.2.2) at 52:54:00:12:35:02 [ether] on enp0s3
[graig@node2 ~]$ ping 10.2.1.11
PING 10.2.1.11 (10.2.1.11) 56(84) bytes of data.
64 bytes from 10.2.1.11: icmp_seq=1 ttl=64 time=0.489 ms
64 bytes from 10.2.1.11: icmp_seq=2 ttl=64 time=0.569 ms
64 bytes from 10.2.1.11: icmp_seq=3 ttl=64 time=0.479 ms
64 bytes from 10.2.1.11: icmp_seq=4 ttl=64 time=0.800 ms
64 bytes from 10.2.1.11: icmp_seq=5 ttl=64 time=0.653 ms
^C
--- 10.2.1.11 ping statistics ---
5 packets transmitted, 5 received, 0% packet loss, time 4123ms
rtt min/avg/max/mdev = 0.479/0.598/0.800/0.118 ms
[graig@node2 ~]$ 
```

```
user@MacBook-Pro-de-User ~ % scp graig@10.2.1.11:/home/graig/trameNode1.pcap trameNode1
graig@10.2.1.11's password: 
trameNode1.pcap                                 100%   42KB  20.5MB/s   00:00    
```

```
user@MacBook-Pro-de-User ~ % scp graig@10.2.1.12:/home/graig/trameNode2.pcap trameNode2
graig@10.2.1.12's password: 
trameNode2.pcap                                 100%   36KB  11.2MB/s   00:00    
```

| ordre | type trame  | source                   | destination                |
|-------|-------------|--------------------------|----------------------------|
| 224   | ARP | PcsCompu_9a:ca:3f | Broadcast |
| 225   | ARP |  PcsCompu_9a:ca:3f | Broadcast    |
| 226   | ARP        | 0a:00:27:00:00:01   | PcsCompu_9a:ca:3f    |
| 231     | ARP | PcsCompu_11:c0:d6| Broadcast |
| 232     | ARP | PcsCompu_11:c0:d6 | Broadcast   |
| 317  | ARP         | PcsCompu_9a:ca:3f     |      broadcast            |
| 319     |  ARP | PcsCompu_9a:ca:3f    | Broadcast |
| 321     | ARP | PcsCompu_11:c0:d6 | PcsCompu_9a:ca:3f   |
| 323   | ARP    | PcsCompu_11:c0:d6       | PcsCompu_9a:ca:3f          |
| 348     | ARP | PcsCompu_11:c0:d6  | PcsCompu_9a:ca:3f  |
| 349    |  ARP | PcsCompu_9a:ca:3f  | PcsCompu_11:c0:d6   |
| 360   | ARP        | PcsCompu_9a:ca:3f    |       PcsCompu_11:c0:d6                  |
| 361    |  ARP | PcsCompu_11:c0:d6 | PcsCompu_9a:ca:3f   |


| ordre | type trame  | source                   | destination                |
|-------|-------------|--------------------------|----------------------------|
| 3   | ARP | 0a:00:27:00:00:01 | PcsCompu_11:c0:d6 |
| 4   | ARP | PcsCompu_11:c0:d6  | 0a:00:27:00:00:01 |
| 63  | ARP |  PcsCompu_9a:ca:3f | Broadcast    |
| 64   | ARP        |  PcsCompu_9a:ca:3f | Broadcast    |
| 181    | ARP | PcsCompu_11:c0:d6| Broadcast |
| 182     | ARP | PcsCompu_11:c0:d6 | Broadcast   |
| 183  | ARP         |   0a:00:27:00:00:01  |  PcsCompu_11:c0:d6           |
| 215    |  ARP | PcsCompu_9a:ca:3f    | Broadcast |
| 216    | ARP | PcsCompu_11:c0:d6 | PcsCompu_9a:ca:3f   |
| 217   | ARP    | PcsCompu_9a:ca:3f        | Broadcast         |
| 218     | ARP | PcsCompu_11:c0:d6  | PcsCompu_9a:ca:3f  |
| 229   |  ARP | PcsCompu_11:c0:d6  |  PcsCompu_9a:ca:3f  |
| 230   | ARP        | PcsCompu_9a:ca:3f    |       PcsCompu_11:c0:d6                  |
| 303   |  ARP | PcsCompu_9a:ca:3f  | PcsCompu_11:c0:d6   |
| 304   | ARP        |  PcsCompu_11:c0:d6   |       PcsCompu_9a:ca:3f               |

# II.Routage

## 1.Mise en place du routage

```
[graig@node1 ~]$ netstat -nr
Table de routage IP du noyau
Destination     Passerelle      Genmask         Indic   MSS Fenêtre irtt Iface
0.0.0.0         10.0.2.2        0.0.0.0         UG        0 0          0 enp0s3
10.0.2.0        0.0.0.0         255.255.255.0   U         0 0          0 enp0s3
10.2.1.0        0.0.0.0         255.255.255.0   U         0 0          0 enp0s8
[graig@node1 ~]$ ip route show
default via 10.0.2.2 dev enp0s3 proto dhcp metric 100
10.0.2.0/24 dev enp0s3 proto kernel scope link src 10.0.2.15 metric 100
10.2.1.0/24 dev enp0s8 proto kernel scope link src 10.2.1.12 metric 101
[graig@node1 ~]$ sudo ifdown enp0s3
[sudo] Mot de passe de graig :
Connexion « enp0s3 » désactivée (chemin D-Bus actif : /org/freedesktop/NetworkManager/ActiveConnection/1)
[graig@node1 ~]$ ip route show
10.2.1.0/24 dev enp0s8 proto kernel scope link src 10.2.1.12 metric 101
[graig@node1 ~]$ sudo firewall-cmd --list-all
public (active)
  target: default
  icmp-block-inversion: no
  interfaces: enp0s8
  sources:
  services: cockpit dhcpv6-client ssh
  ports:
  protocols:
  masquerade: no
  forward-ports:
  source-ports:
  icmp-blocks:
  rich rules:
[graig@node1 ~]$ sudo firewall-cmd --get-active-zone
public
  interfaces: enp0s8
[graig@node1 ~]$ sudo firewall-cmd --add-masquerade --zone=public
success
[graig@node1 ~]$ sudo firewall-cmd --add-masquerade --zone=public --permanent
success
```

```
[graig@node1 ~]$ ping 10.2.1.254
PING 10.2.1.254 (10.2.1.254) 56(84) bytes of data.
64 bytes from 10.2.1.254: icmp_seq=1 ttl=64 time=0.765 ms
64 bytes from 10.2.1.254: icmp_seq=2 ttl=64 time=0.633 ms
```

```
[graig@marcel ~]$ ping 10.2.2.254
PING 10.2.2.254 (10.2.2.254) 56(84) bytes of data.
64 bytes from 10.2.2.254: icmp_seq=1 ttl=64 time=1.33 ms
64 bytes from 10.2.2.254: icmp_seq=2 ttl=64 time=0.508 ms
64 bytes from 10.2.2.254: icmp_seq=3 ttl=64 time=0.765 ms
```

## 2.Analyse de trames


- reinitilisation des tables arp 
```
[graig@node1 ~]$ sudo ip neigh flush all
[graig@router ~]$ sudo ip neigh flush all
[graig@marcel ~]$ sudo ip neigh flush all
```

```
[graig@node1 ~]$ sudo tcpdump -i enp0s8 -w mescouilles.pcap port not 22
[sudo] Mot de passe de graig : 
dropped privs to tcpdump
tcpdump: listening on enp0s8, link-type EN10MB (Ethernet), capture size 262144 bytes

[graig@router ~]$ arp -a
? (10.2.2.1) at 0a:00:27:00:00:03 [ether] on enp0s9
? (10.2.2.1) at 0a:00:27:00:00:03 [ether] on enp0s9

[graig@node1 ~]$ ping 10.2.2.12
PING 10.2.2.12 (10.2.2.12) 56(84) bytes of data.
64 bytes from 10.2.2.12: icmp_seq=1 ttl=63 time=1.49 ms
64 bytes from 10.2.2.12: icmp_seq=2 ttl=63 time=0.924 ms

[graig@router ~]$ arp -a
? (10.2.1.11) at 08:00:27:11:c0:d6 [ether] on enp0s8
? (10.2.2.12) at 08:00:27:0e:ce:30 [ether] on enp0s9
_gateway (10.0.2.2) at 52:54:00:12:35:02 [ether] on enp0s3
? (10.2.2.1) at 0a:00:27:00:00:03 [ether] on enp0s9
```
- le routeur met a jour sa table arp 


- ping avec node1 / marcel ecoute 

```
[graig@node1 ~]$ ping 10.2.2.12
PING 10.2.2.12 (10.2.2.12) 56(84) bytes of data.
64 bytes from 10.2.2.12: icmp_seq=1 ttl=63 time=1.02 ms
64 bytes from 10.2.2.12: icmp_seq=2 ttl=63 time=1.03 ms
64 bytes from 10.2.2.12: icmp_seq=3 ttl=63 time=1.78 ms
64 bytes from 10.2.2.12: icmp_seq=4 ttl=63 time=1.06 ms

[graig@marcel ~]$ sudo tcpdump not host 10.2.2.1 -c 10 
dropped privs to tcpdump
tcpdump: verbose output suppressed, use -v or -vv for full protocol decode
listening on enp0s8, link-type EN10MB (Ethernet), capture size 262144 bytes
16:16:10.049116 IP 10.2.2.254 > marcel.net2.tp2: ICMP echo request, id 1634, seq 1, length 64
16:16:10.049266 IP marcel.net2.tp2 > 10.2.2.254: ICMP echo reply, id 1634, seq 1, length 64
16:16:11.052475 IP 10.2.2.254 > marcel.net2.tp2: ICMP echo request, id 1634, seq 2, length 64
16:16:11.052535 IP marcel.net2.tp2 > 10.2.2.254: ICMP echo reply, id 1634, seq 2, length 64
16:16:12.053292 IP 10.2.2.254 > marcel.net2.tp2: ICMP echo request, id 1634, seq 3, length 64
16:16:12.053338 IP marcel.net2.tp2 > 10.2.2.254: ICMP echo reply, id 1634, seq 3, length 64
16:16:13.054676 IP 10.2.2.254 > marcel.net2.tp2: ICMP echo request, id 1634, seq 4, length 64
16:16:13.054718 IP marcel.net2.tp2 > 10.2.2.254: ICMP echo reply, id 1634, seq 4, length 64
16:16:15.117275 ARP, Request who-has marcel.net2.tp2 tell 10.2.2.254, length 46
16:16:15.117307 ARP, Reply marcel.net2.tp2 is-at 08:00:27:0e:ce:30 (oui Unknown), length 28
10 packets captured
10 packets received by filter
0 packets dropped by kernel
```
- j'ai mis sur ecoute marcel et avec node1 j'ai ping j'ai recu le ping sur node1 et le pong sur marcel 
- j'ai enlever "host 10.2.2.1" <- ip du virtualiseur il empechait l'affichage de la reception du ping 

- marcel ping node 1 / node 1 ecoute 

```
[graig@node1 ~]$ sudo tcpdump not host 10.2.1.1 -c 10
dropped privs to tcpdump
tcpdump: verbose output suppressed, use -v or -vv for full protocol decode
listening on enp0s8, link-type EN10MB (Ethernet), capture size 262144 bytes
16:33:40.248734 IP 10.2.1.254 > node1.net1.tp2: ICMP echo request, id 1714, seq 1, length 64
16:33:40.248805 IP node1.net1.tp2 > 10.2.1.254: ICMP echo reply, id 1714, seq 1, length 64
16:33:41.249924 IP 10.2.1.254 > node1.net1.tp2: ICMP echo request, id 1714, seq 2, length 64
16:33:41.249972 IP node1.net1.tp2 > 10.2.1.254: ICMP echo reply, id 1714, seq 2, length 64
16:33:42.250869 IP 10.2.1.254 > node1.net1.tp2: ICMP echo request, id 1714, seq 3, length 64
16:33:42.250911 IP node1.net1.tp2 > 10.2.1.254: ICMP echo reply, id 1714, seq 3, length 64
16:33:43.252881 IP 10.2.1.254 > node1.net1.tp2: ICMP echo request, id 1714, seq 4, length 64
16:33:43.252923 IP node1.net1.tp2 > 10.2.1.254: ICMP echo reply, id 1714, seq 4, length 64
16:33:45.439689 ARP, Request who-has node1.net1.tp2 tell 10.2.1.254, length 46
16:33:45.439710 ARP, Reply node1.net1.tp2 is-at 08:00:27:11:c0:d6 (oui Unknown), length 28
10 packets captured
10 packets received by filter
0 packets dropped by kernel


[graig@marcel ~]$ ping 10.2.1.11
PING 10.2.1.11 (10.2.1.11) 56(84) bytes of data.
64 bytes from 10.2.1.11: icmp_seq=1 ttl=63 time=1.05 ms
64 bytes from 10.2.1.11: icmp_seq=2 ttl=63 time=0.990 ms
64 bytes from 10.2.1.11: icmp_seq=3 ttl=63 time=1.19 ms
64 bytes from 10.2.1.11: icmp_seq=4 ttl=63 time=1.11 ms
^C
--- 10.2.1.11 ping statistics ---
4 packets transmitted, 4 received, 0% packet loss, time 3004ms
rtt min/avg/max/mdev = 0.990/1.086/1.191/0.081 ms

```

- routeur ping / node1 ecoute 

```
[graig@node1 ~]$ sudo tcpdump not host 10.2.1.1 -c 10
[sudo] Mot de passe de graig : 
dropped privs to tcpdump
tcpdump: verbose output suppressed, use -v or -vv for full protocol decode
listening on enp0s8, link-type EN10MB (Ethernet), capture size 262144 bytes
16:29:55.733649 IP 10.2.1.254 > node1.net1.tp2: ICMP echo request, id 1599, seq 1, length 64
16:29:55.733732 IP node1.net1.tp2 > 10.2.1.254: ICMP echo reply, id 1599, seq 1, length 64
16:29:56.735481 IP 10.2.1.254 > node1.net1.tp2: ICMP echo request, id 1599, seq 2, length 64
16:29:56.735527 IP node1.net1.tp2 > 10.2.1.254: ICMP echo reply, id 1599, seq 2, length 64
16:29:57.789243 IP 10.2.1.254 > node1.net1.tp2: ICMP echo request, id 1599, seq 3, length 64
16:29:57.789306 IP node1.net1.tp2 > 10.2.1.254: ICMP echo reply, id 1599, seq 3, length 64
16:30:00.896744 ARP, Request who-has 10.2.1.254 tell node1.net1.tp2, length 28
16:30:00.897244 ARP, Reply 10.2.1.254 is-at 08:00:27:9a:ca:3f (oui Unknown), length 46
16:30:01.179314 ARP, Request who-has node1.net1.tp2 tell 10.2.1.254, length 46
16:30:01.179343 ARP, Reply node1.net1.tp2 is-at 08:00:27:11:c0:d6 (oui Unknown), length 28
10 packets captured
10 packets received by filter
0 packets dropped by kernel


[graig@router ~]$ ping 10.2.1.11
PING 10.2.1.11 (10.2.1.11) 56(84) bytes of data.
64 bytes from 10.2.1.11: icmp_seq=1 ttl=64 time=0.727 ms
64 bytes from 10.2.1.11: icmp_seq=2 ttl=64 time=0.459 ms
64 bytes from 10.2.1.11: icmp_seq=3 ttl=64 time=0.793 ms
^C
--- 10.2.1.11 ping statistics ---
3 packets transmitted, 3 received, 0% packet loss, time 2056ms
rtt min/avg/max/mdev = 0.459/0.659/0.793/0.147 ms
```

- routeur ping / marcel ecoute 

```
[graig@router ~]$ ping 10.2.2.12
PING 10.2.2.12 (10.2.2.12) 56(84) bytes of data.
64 bytes from 10.2.2.12: icmp_seq=1 ttl=64 time=0.881 ms
64 bytes from 10.2.2.12: icmp_seq=2 ttl=64 time=0.617 ms
64 bytes from 10.2.2.12: icmp_seq=3 ttl=64 time=0.559 ms
^C
--- 10.2.2.12 ping statistics ---
3 packets transmitted, 3 received, 0% packet loss, time 2061ms
rtt min/avg/max/mdev = 0.559/0.685/0.881/0.143 ms


[graig@marcel ~]$ sudo tcpdump not host 10.2.2.1 -c 10
[sudo] Mot de passe de graig : 
dropped privs to tcpdump
tcpdump: verbose output suppressed, use -v or -vv for full protocol decode
listening on enp0s8, link-type EN10MB (Ethernet), capture size 262144 bytes
16:31:17.120307 IP 10.2.2.254 > marcel.net2.tp2: ICMP echo request, id 1609, seq 1, length 64
16:31:17.120514 IP marcel.net2.tp2 > 10.2.2.254: ICMP echo reply, id 1609, seq 1, length 64
16:31:18.156111 IP 10.2.2.254 > marcel.net2.tp2: ICMP echo request, id 1609, seq 2, length 64
16:31:18.156156 IP marcel.net2.tp2 > 10.2.2.254: ICMP echo reply, id 1609, seq 2, length 64
16:31:19.181404 IP 10.2.2.254 > marcel.net2.tp2: ICMP echo request, id 1609, seq 3, length 64
16:31:19.181465 IP marcel.net2.tp2 > 10.2.2.254: ICMP echo reply, id 1609, seq 3, length 64
16:31:22.379630 ARP, Request who-has marcel.net2.tp2 tell 10.2.2.254, length 46
16:31:22.379656 ARP, Reply marcel.net2.tp2 is-at 08:00:27:0e:ce:30 (oui Unknown), length 28
16:31:22.583418 ARP, Request who-has 10.2.2.254 tell marcel.net2.tp2, length 28
16:31:22.584728 ARP, Reply 10.2.2.254 is-at 08:00:27:a7:15:cf (oui Unknown), length 46
10 packets captured
10 packets received by filter
0 packets dropped by kernel
```

- ecoute : sudo tcpdump not host < l'ip de la virtualbox : carte reseau /> -c 10
- puis ping la machine de l'autre coté du routeur.

- Tableau node1 :

| ordre | type trame  | IP source | MAC source                | IP destination | MAC destination            |
|-------|-------------|-----------|---------------------------|----------------|----------------------------|
| 1     | ARP         | PcsCompu_11:c0:d6 | 08:00:27:11:c0:d6 | Broadcast       | ff:ff:ff:ff:ff:ff         |
| 2     | ARP         | PcsCompu_11:c0:d6 | 08:00:27:11:c0:d6 | Broadcast       | ff:ff:ff:ff:ff:ff         |
| 3     | ARP         | PcsCompu_0a:ca:3f | 08:00:27:9a:ca:3f | PcsCompu_11:c0:d6 | 08:00:27:11:c0:d6       |
| 4     | IMCP: Ping request  | 10.2.2.11 | 08:00:27:11:c0:d6 | 10.2.2.12         | 08:00:27:9a:ca:3f       |
| 5     | ARP         | PcsCompu_0a:ca:3f | 08:00:27:9a:ca:3f | PcsCompu_11:c0:d6 | 08:00:27:11:c0:d6       |
| 6     | IMCP: Ping reply    | 10.2.2.12 | 08:00:27:9a:ca:3f | 10.2.1.11         | 08:00:27:11:c0:d6       |
| 7     | IMCP: Ping request  | 10.2.1.11 | 08:00:27:11:c0:d6 | 10.2.2.12         | 08:00:27:9a:ca:3f       |
| 8     | IMCP: Ping reply    | 10.2.2.12 | 08:00:27:9a:ca:3f | 10.2.1.11         | 08:00:27:11:c0:d6       |
| 9     | IMCP: Ping request  | 10.2.2.11 | 08:00:27:11:c0:d6 | 10.2.2.12         | 08:00:27:9a:ca:3f       |
| 10    | IMCP: Ping reply    | 10.2.2.12 | 08:00:27:9a:ca:3f | 10.2.1.11         | 08:00:27:11:c0:d6       |
| 11    | IMCP: Ping request  | 10.2.2.11 | 08:00:27:11:c0:d6 | 10.2.2.12         | 08:00:27:9a:ca:3f       |
| 12    | IMCP: Ping reply    | 10.2.2.12 | 08:00:27:9a:ca:3f | 10.2.1.11         | 08:00:27:11:c0:d6       |
| 13    | IMCP: Ping request  | 10.2.2.11 | 08:00:27:11:c0:d6 | 10.2.2.12         | 08:00:27:9a:ca:3f       |
| 14    | IMCP: Ping reply    | 10.2.2.12 | 08:00:27:9a:ca:3f | 10.2.1.11         | 08:00:27:11:c0:d6       |
| 15    | ARP         | PcsCompu_0a:ca:3f | 08:00:27:9a:ca:3f | PcsCompu_11:c0:d6 | 08:00:27:11:c0:d6       |
| 16    | ARP         | PcsCompu_11:c0:d6 | 08:00:27:11:c0:d6 | PcsCompu_9a:ca:3f | 08:00:27:9a:ca:3f       |

- j'ai mis en Mac destination les infos car en vérité je n'ai pas les adresses MAC dans l'échange entre node1 et marcel a travers le router
- ou alors y'a une manip ou une cmd que j'ai pas trouver ou pas faite.

-Tableau marcel :

| ordre | type trame  | IP source | MAC source                | IP destination | MAC destination            |
|-------|-------------|-----------|---------------------------|----------------|----------------------------|
| 1     | ARP  | PcsCompu_a7:15:cf | 08:00:27:a7:15:cf        | Broadcast         | ff:ff:ff:ff:ff:ff       |
| 2     | ARP  | PcsCompu_0e:ce:30 | 08:00:27:0e:ce:30        | PcsCompu_a7:15:cf | 08:00:27:a7:15:cf       |
| 3     | ARP  | PcsCompu_a7:15:cf | 08:00:27:a7:15:cf        | Broadcast         | ff:ff:ff:ff:ff:ff       |
| 4     | ARP  | PcsCompu_0e:ce:30 | 08:00:27:0e:ce:30        | PcsCompu_a7:15:cf | 08:00:27:a7:15:cf       |
| 5     | ICMP | 10.2.2.254        | 08:00:27:a7:15:cf        | 10.2.2.12         | 08:00:27:0e:ce:30       |
| 6     | ICMP | 10.2.2.12         | 08:00:27:0e:ce:30        | 10.2.2.254        | 08:00:27:a7:15:cf       |
| 7     | ICMP | 10.2.2.254        | 08:00:27:a7:15:cf        | 10.2.2.12         | 08:00:27:0e:ce:30       |
| 8     | ICMP | 10.2.2.12         | 08:00:27:0e:ce:30        | 10.2.2.254        | 08:00:27:a7:15:cf       |
| 9     | ICMP | 10.2.2.254        | 08:00:27:a7:15:cf        | 10.2.2.12         | 08:00:27:0e:ce:30       |
| 10    | ICMP | 10.2.2.12         | 08:00:27:0e:ce:30        | 10.2.2.254        | 08:00:27:a7:15:cf       |
| 11    | ICMP | 10.2.2.254        | 08:00:27:a7:15:cf        | 10.2.2.12         | 08:00:27:0e:ce:30       |
| 12    | ICMP | 10.2.2.12         | 08:00:27:0e:ce:30        | 10.2.2.254        | 08:00:27:a7:15:cf       |
| 13    | ICMP | 10.2.2.254        | 08:00:27:a7:15:cf        | 10.2.2.12         | 08:00:27:0e:ce:30       |
| 14    | ICMP | 10.2.2.12         | 08:00:27:0e:ce:30        | 10.2.2.254        | 08:00:27:a7:15:cf       |
| 15    | ARP  | PcsCompu_0e:ce:30 | 08:00:27:0e:ce:30        | PcsCompu_a7:15:cf | 08:00:27:a7:15:cf       |
| 16    | ARP  | PcsCompu_a7:15:cf | 08:00:27:a7:15:cf        | PcsCompu_0e:ce:30 | 08:00:27:0e:ce:30       |

- toujours pas les adresses MAC je pense que c'est les commandes que je tape il doit manquer un paramètre ...


## 3. Acces a Internet

```
[graig@node1 ~]$ ip r a 10.2.2.0/24 via 10.2.1.254 dev enp0s8
RTNETLINK answers: Operation not permitted
[graig@node1 ~]$ sudo !!
sudo ip r a 10.2.2.0/24 via 10.2.1.254 dev enp0s8
[sudo] Mot de passe de graig : 
[graig@node1 ~]$ ip r
10.2.1.0/24 dev enp0s8 proto kernel scope link src 10.2.1.11 metric 100 
10.2.2.0/24 via 10.2.1.254 dev enp0s8 
10.2.2.0/24 via 10.2.1.254 dev enp0s8 proto static metric 100 
```

```
[graig@marcel ~]$ sudo ip r a 10.2.1.0/24 via 10.2.2.254 dev enp0s8
[graig@marcel ~]$ ip r
10.2.1.0/24 via 10.2.2.254 dev enp0s8 
10.2.2.0/24 dev enp0s8 proto kernel scope link src 10.2.2.12 metric 100 
```

```
[graig@node1 ~]$ sudo nano /etc/sysconfig/network
[graig@node1 ~]$ sudo nmcli con reload
[graig@node1 ~]$ sudo nmcli con up enp0s8
Connexion activée (chemin D-Bus actif : /org/freedesktop/NetworkManager/ActiveConnection/3)
[graig@node1 ~]$ ip r s
default via 10.2.1.254 dev enp0s8 proto static metric 100 
10.2.1.0/24 dev enp0s8 proto kernel scope link src 10.2.1.11 metric 100 
10.2.2.0/24 via 10.2.1.254 dev enp0s8 proto static metric 100 
[graig@node1 ~]$ ping 8.8.8.8
PING 8.8.8.8 (8.8.8.8) 56(84) bytes of data.
64 bytes from 8.8.8.8: icmp_seq=1 ttl=61 time=20.2 ms
64 bytes from 8.8.8.8: icmp_seq=2 ttl=61 time=18.10 ms
64 bytes from 8.8.8.8: icmp_seq=3 ttl=61 time=18.6 ms
^C
--- 8.8.8.8 ping statistics ---
3 packets transmitted, 3 received, 0% packet loss, time 2003ms
rtt min/avg/max/mdev = 18.626/19.268/20.183/0.673 ms
```

```
[graig@node1 ~]$ sudo nano /etc/resolv.conf
[graig@node1 ~]$ dig google.com

; <<>> DiG 9.11.26-RedHat-9.11.26-4.el8_4 <<>> google.com
;; global options: +cmd
;; Got answer:
;; ->>HEADER<<- opcode: QUERY, status: NOERROR, id: 1165
;; flags: qr rd ra; QUERY: 1, ANSWER: 1, AUTHORITY: 0, ADDITIONAL: 1

;; OPT PSEUDOSECTION:
; EDNS: version: 0, flags:; udp: 4000
;; QUESTION SECTION:
;google.com.			IN	A

;; ANSWER SECTION:
google.com.		76	IN	A	216.58.206.238

;; Query time: 4 msec
;; SERVER: 10.33.10.2#53(10.33.10.2)
;; WHEN: lun. sept. 27 11:24:15 CEST 2021
;; MSG SIZE  rcvd: 55
```

```
[graig@marcel ~]$ sudo nano /etc/sysconfig/network
[sudo] Mot de passe de graig : 
Désolé, essayez de nouveau.
[sudo] Mot de passe de graig : 
[graig@marcel ~]$ sudo nmcli con reload
[graig@marcel ~]$ sudo nmcli con up enp0s8
Connexion activée (chemin D-Bus actif : /org/freedesktop/NetworkManager/ActiveConnection/2)
[graig@marcel ~]$ ip r s
default via 10.2.2.254 dev enp0s8 proto static metric 100 
10.2.2.0/24 dev enp0s8 proto kernel scope link src 10.2.2.12 metric 100 
[graig@marcel ~]$ ping 8.8.8.8
PING 8.8.8.8 (8.8.8.8) 56(84) bytes of data.
64 bytes from 8.8.8.8: icmp_seq=1 ttl=61 time=79.2 ms
64 bytes from 8.8.8.8: icmp_seq=2 ttl=61 time=46.2 ms
^C
--- 8.8.8.8 ping statistics ---
2 packets transmitted, 2 received, 0% packet loss, time 1002ms
rtt min/avg/max/mdev = 46.240/62.724/79.209/16.486 ms
[graig@marcel ~]$ sudo nano /etc/resolv.conf
[graig@marcel ~]$ dig google.com

; <<>> DiG 9.11.26-RedHat-9.11.26-4.el8_4 <<>> google.com
;; global options: +cmd
;; Got answer:
;; ->>HEADER<<- opcode: QUERY, status: NOERROR, id: 46836
;; flags: qr rd ra; QUERY: 1, ANSWER: 1, AUTHORITY: 0, ADDITIONAL: 1

;; OPT PSEUDOSECTION:
; EDNS: version: 0, flags:; udp: 4000
;; QUESTION SECTION:
;google.com.			IN	A

;; ANSWER SECTION:
google.com.		132	IN	A	216.58.206.238

;; Query time: 28 msec
;; SERVER: 10.33.10.2#53(10.33.10.2)
;; WHEN: lun. sept. 27 11:27:26 CEST 2021
;; MSG SIZE  rcvd: 55
```

```
[graig@node1 ~]$ ping google.com
PING google.com (216.58.206.238) 56(84) bytes of data.
64 bytes from par10s34-in-f14.1e100.net (216.58.206.238): icmp_seq=1 ttl=61 time=103 ms
64 bytes from par10s34-in-f14.1e100.net (216.58.206.238): icmp_seq=2 ttl=61 time=22.0 ms
^C
--- google.com ping statistics ---
2 packets transmitted, 2 received, 0% packet loss, time 1002ms
rtt min/avg/max/mdev = 22.023/62.709/103.395/40.686 ms
```

```
[graig@marcel ~]$ ping google.com
PING google.com (216.58.206.238) 56(84) bytes of data.
64 bytes from par10s34-in-f14.1e100.net (216.58.206.238): icmp_seq=1 ttl=61 time=31.1 ms
64 bytes from par10s34-in-f14.1e100.net (216.58.206.238): icmp_seq=2 ttl=61 time=36.3 ms
64 bytes from par10s34-in-f14.1e100.net (216.58.206.238): icmp_seq=3 ttl=61 time=113 ms
^C
--- google.com ping statistics ---
3 packets transmitted, 3 received, 0% packet loss, time 2006ms
rtt min/avg/max/mdev = 31.109/60.097/112.906/37.401 ms
```

```
[graig@node1 ~]$ ping 8.8.8.8
PING 8.8.8.8 (8.8.8.8) 56(84) bytes of data.
64 bytes from 8.8.8.8: icmp_seq=1 ttl=61 time=40.5 ms
64 bytes from 8.8.8.8: icmp_seq=2 ttl=61 time=118 ms
64 bytes from 8.8.8.8: icmp_seq=3 ttl=61 time=127 ms
64 bytes from 8.8.8.8: icmp_seq=4 ttl=61 time=39.10 ms
64 bytes from 8.8.8.8: icmp_seq=5 ttl=61 time=45.7 ms
^C
--- 8.8.8.8 ping statistics ---
5 packets transmitted, 5 received, 0% packet loss, time 4007ms
rtt min/avg/max/mdev = 39.958/74.209/127.307/39.575 ms

[graig@node1 ~]$ sudo ip neigh flush all
[graig@node1 ~]$ sudo tcpdump -i enp0s8 -w trameNode1-2.pcap
dropped privs to tcpdump
tcpdump: listening on enp0s8, link-type EN10MB (Ethernet), capture size 262144 bytes
^C38 packets captured
39 packets received by filter
0 packets dropped by kernel
```


| ordre | type trame  | IP source | MAC source                | IP destination | MAC destination            |
|-------|-------------|-----------|---------------------------|----------------|----------------------------|
| 1     | ping    | node1: 10.2.1.11 | 08:00:27:11:c0:d6        | 8.8.8.8        | google: 08:00:27:9a:ca:3f  |
| 2     | pong    | google: 1.1.1.1 | google: 08:00:27:9a:ca:3f | 10.2.1.11  | 08:00:27:11:c0:d6       |

# III DHCP

## 1.mise en place du DCHP

```
[graig@node1 ~]$ sudo dnf install dhcp-server
Dernière vérification de l’expiration des métadonnées effectuée il y a 0:00:48 le lun. 27 sept. 2021 12:31:59 CEST.
Dépendances résolues.
==========================================================================================================
 Paquet                   Architecture        Version                           Dépôt               Taille
==========================================================================================================
Installation:
 dhcp-server              x86_64              12:4.3.6-44.el8_4.1               baseos              529 k

Résumé de la transaction
==========================================================================================================
Installer  1 Paquet

Taille totale des téléchargements : 529 k
Taille des paquets installés : 1.2 M
Voulez-vous continuer ? [o/N] : o
Téléchargement des paquets :
dhcp-server-4.3.6-44.el8_4.1.x86_64.rpm                                   758 kB/s | 529 kB     00:00    
----------------------------------------------------------------------------------------------------------
Total                                                                     548 kB/s | 529 kB     00:00     
Test de la transaction
La vérification de la transaction a réussi.
Lancement de la transaction de test
Transaction de test réussie.
Exécution de la transaction
  Préparation           :                                                                             1/1 
  Exécution du scriptlet: dhcp-server-12:4.3.6-44.el8_4.1.x86_64                                      1/1 
  Installation          : dhcp-server-12:4.3.6-44.el8_4.1.x86_64                                      1/1 
attention : /etc/sysconfig/dhcpd créé en tant que /etc/sysconfig/dhcpd.rpmnew

  Exécution du scriptlet: dhcp-server-12:4.3.6-44.el8_4.1.x86_64                                      1/1 
  Vérification de       : dhcp-server-12:4.3.6-44.el8_4.1.x86_64                                      1/1 

Installé:
  dhcp-server-12:4.3.6-44.el8_4.1.x86_64                                                                  

Terminé !
```

```
[graig@node1 ~]$ sudo nano /etc/dhcp/dhcpd.conf

default-lease-time 900;
max-lease-time 10800;
ddns-update-style none;
authoritative;
subnet 10.2.1.0 netmask 255.255.255.0 {
  range 10.2.1.50 10.2.1.100;
  option routers 10.2.1.11;
  option subnet-mask 255.255.255.0;
  option domain-name-servers 1.1.1.1;
}
```

## Analyse des trames

